<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_toko extends CI_Model {
	public function getDataBuku()
	{
		return $this->db->join('kategori','kategori.id_kategori=buku.id_kategori')
						->get('buku')
						->result();
	}
	public function getDataKategori()
	{
		return $this->db->get('kategori')->result();
	}
	public function tampil_kategori(){
		return $this->db->get('kategori')->result();
	}
	public function simpan_buku($nama_file){
		if($nama_file==""){
			$object=array(
				'judul_buku'=>$this->input->post('judul_buku'),
				'tahun'=>$this->input->post('tahun'),
				'harga'=>$this->input->post('harga'),
				'penerbit'=>$this->input->post('penerbit'),
				'penulis'=>$this->input->post('penulis'),
				'stok'=>$this->input->post('stok'),
				'id_kategori'=>$this->input->post('kategori'),
				);
		}else{
			$object=array(
				'judul_buku'=>$this->input->post('judul_buku'),
				'tahun'=>$this->input->post('tahun'),
				'harga'=>$this->input->post('harga'),
				'cover'=>$nama_file,
				'penerbit'=>$this->input->post('penerbit'),
				'penulis'=>$this->input->post('penulis'),
				'stok'=>$this->input->post('stok'),
				'id_kategori'=>$this->input->post('kategori'),
				);
			}
			return $this->db->insert('buku',$object);
		}
	public function hapus_buku($id='')
	{
		return $this->db->where('id_buku', $id)->delete('buku');
	}
	public function detail($a){
				$tm_buku= $this->db
						->join('kategori','kategori.id_kategori=buku.id_kategori')
						->where('id_buku',$a)
						->get('buku')
						->row();
				return $tm_buku;
	}
	public function buku_update_no_foto()
	{
		$object=array(
				'judul_buku'=>$this->input->post('judul_buku'),
				'tahun'=>$this->input->post('tahun'),
				'harga'=>$this->input->post('harga'),
				'penerbit'=>$this->input->post('penerbit'),
				'penulis'=>$this->input->post('penulis'),
				'stok'=>$this->input->post('stok'),
				'id_kategori'=>$this->input->post('kategori')
		);

		return $this->db->where('id_buku', $this->input->post('id_buku'))
						->update('buku', $object);

	}
	public function buku_update_dengan_foto($nama_foto='')
	{
		$object=array(
				'judul_buku'=>$this->input->post('judul_buku'),
				'tahun'=>$this->input->post('tahun'),
				'harga'=>$this->input->post('harga'),
				'cover'=>$nama_foto,
				'penerbit'=>$this->input->post('penerbit'),
				'penulis'=>$this->input->post('penulis'),
				'stok'=>$this->input->post('stok'),
				'id_kategori'=>$this->input->post('kategori')
				);
		return $this->db->where('id_buku', $this->input->post('id_buku'))
						->update('buku', $object);
	}
}


/* End of file M_toko.php */
/* Location: ./application/models/M_toko.php */