<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_histori extends CI_Model {

	public function getDatahistor()
	{
		return $this->db->join('transaksi','transaksi.id_transaksi=nota.id_transaksi')						
						->get('nota')->result();
	}

	public function hapus($id_transaksi)
	{
		return $this->db->where('id_transaksi', $id_transaksi)->delete('transaksi');
	}

}

/* End of file M_histori.php */
/* Location: ./application/models/M_histori.php */