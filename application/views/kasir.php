<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Jquery DataTable | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="<?=base_url()?>assets/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?=base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?=base_url()?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?=base_url()?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?=base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?=base_url()?>assets/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>          
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Data Kasir
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <?= $this->session->flashdata('pesan')?>
                                <center><a href="#tambah" data-toggle="modal" class="btn btn-warning">Tambah</a> </center>
                                <br>
                                <table id="example" class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                         <tr>
                                            <th>Id Kasir</th>
                                            <th>Nama Kasir</th>
                                            <th>Password</th>
                                            <th>Level</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                    
                                        <?php $no=0 ;foreach ($tampil_kasir as $kasir):$no++; ?>
                                            <tr>
                                                    <td><?=$kasir->id_kasir?></td> 
                                                    <td><?=$kasir->nama_kasir?></td>
                                                    <td><?=$kasir->password?></td>
                                                    <td><?=$kasir->level?></td>
                                                    <td>
                                                        <a href="#edit" onclick="edit('<?=$kasir->id_kasir?>')" data-toggle="modal" class="btn btn-success">Ubah</a>
                                                        <a href="<?=base_url('index.php/kasir/hapus/'.$kasir->id_kasir)?>" onclick="return confir('Apakah Anda Yakin')" class="btn btn-danger">Hapus</a>
                                                    </td>
                                                    
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                                <div class="modal fade" id="tambah">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="model-title"> Tambah Kasir</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="<?=base_url('index.php/kasir/tambah_kasir')?>" method="post">
                                                    <table>
                                                        <tr>
                                                            <td>Nama Kasir</td><td><input type="text" name="nama_kasir" required class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Level Kasir</td><td><input type="text" name="level" required class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Password</td><td><input type="text" name="password" required class="form-control"></td>
                                                        </tr>
                                                    </table>
                                                    <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="modal fade" id="edit">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="model-title"> Tambah Kasir</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="<?=base_url('index.php/kasir/ubah')?>" method="post">
                                                    <input type="hidden" name="id_kasir" id="id_kasir">
                                                    <table>
                                                        <tr>
                                                            <td>Nama kasir</td><td><input type="text" name="nama_kasir" id="nama_kasir" required class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Level kasir</td><td><input type="text" name="level" id="level" required class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Password</td><td><input type="text" name="password" id="password" required class="form-control"></td>
                                                        </tr>
                                                    </table>
                                                    <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $(document).ready(function () {
                    $('#example').DataTable();
                });
            </script>

            <script type="text/javascript">
                function edit(a) {
                    $.ajax({
                        type:"post",
                        url:"<?=base_url()?>index.php/kasir/edit_kasir/"+a,
                        dataType:"json",
                        success:function (data) {
                            $("#id_kasir").val(data.id_kasir);
                            $("#nama_kasir").val(data.nama_kasir);
                            $("#id_kasir_lama").val(data.id_kasir);
                        }
                    });
                }
            </script>
            <!-- #END# Exportable Table -->
    <!-- Jquery Core Js -->
    <script src="<?=base_url()?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?=base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?=base_url()?>assets/js/admin.js"></script>
    <script src="<?=base_url()?>assets/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="<?=base_url()?>assets/js/demo.js"></script>
</body>

</html>