<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>UKL</title>
    <!-- Favicon-->
    <link rel="icon" href="<?=base_url()?>asset/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?=base_url()?>asset/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?=base_url()?>asset/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?=base_url()?>asset/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?=base_url()?>asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?=base_url()?>asset/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?=base_url()?>asset/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>          
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Histori Pembelian
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <?= $this->session->flashdata('pesan')?>
                                <br>
                                <table id="example" class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                         <tr>
                                            <th>Id Transaksi</th>
                                            <th>Nama Pembeli</th>
                                            <th>Total</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                    
                                        <?php $no=0 ;foreach ($tampil_histori as $histori):$no++; ?>
                                            <tr>
                                                    <td><?=$histori->id_transaksi?></td> 
                                                    <td><?=$histori->nama_pembeli?></td>
                                                    <td><?=$histori->total?></td>
                                                    <td><?=$histori->tanggal_beli?></td>
                                                    
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                                <div class="modal fade" id="tambah">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="model-title"> Tambah histori</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="<?=base_url('index.php/histori/tambah_histori')?>" method="post">
                                                    <table>
                                                        <tr>
                                                            <td>Nama histori</td><td><input type="text" name="nama_histori" required class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Level histori</td><td><input type="text" name="level" required class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Password</td><td><input type="text" name="password" required class="form-control"></td>
                                                        </tr>
                                                    </table>
                                                    <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="modal fade" id="edit">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="model-title"> Tambah histori</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="<?=base_url('index.php/histori/ubah')?>" method="post">
                                                    <input type="hidden" name="id_histori" id="id_histori">
                                                    <table>
                                                        <tr>
                                                            <td>Nama histori</td><td><input type="text" name="nama_histori" id="nama_histori" required class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Level histori</td><td><input type="text" name="level" id="level" required class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Password</td><td><input type="text" name="password" id="password" required class="form-control"></td>
                                                        </tr>
                                                    </table>
                                                    <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $(document).ready(function () {
                    $('#example').DataTable();
                });
            </script>

            <script type="text/javascript">
                function edit(a) {
                    $.ajax({
                        type:"post",
                        url:"<?=base_url()?>index.php/histori/edit_histori/"+a,
                        dataType:"json",
                        success:function (data) {
                            $("#id_histori").val(data.id_histori);
                            $("#nama_histori").val(data.nama_histori);
                            $("#id_histori_lama").val(data.id_histori);
                        }
                    });
                }
            </script>
            <!-- #END# Exportable Table -->
    <!-- Jquery Core Js -->
    <script src="<?=base_url()?>asset/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?=base_url()?>asset/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?=base_url()?>asset/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?=base_url()?>asset/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?=base_url()?>asset/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?=base_url()?>asset/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?=base_url()?>asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?=base_url()?>asset/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?=base_url()?>asset/js/admin.js"></script>
    <script src="<?=base_url()?>asset/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="<?=base_url()?>asset/js/demo.js"></script>
</body>

</html>