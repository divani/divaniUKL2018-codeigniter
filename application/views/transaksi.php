<h2 align="center">Halaman Transaksi</h2>
<br>
<div>
	<?php if ($this->session->flashdata('pesan')!=null): ?>
			<div class="alert alert-info alert-dismissible" role="alert" style="background: orange;">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?=$this->session->flashdata('pesan')?>
			</div>
		<?php endif ?>	
		<?php if ($this->session->flashdata('pesan_print')!=null): ?>
			<div class="alert alert-info alert-dismissible" role="alert" style="background: orange;">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?=$this->session->flashdata('pesan_print')?>
			</div>
		<?php endif ?>
	<table id="example" class="table table-hover table-striped">
		<thead>
			<tr>
				<th>NO</th><th>Nama Buku</th><th>Tahun</th><th>Harga</th><th>Cover</th><th>Penerbit</th><th>Penulis</th><th>Stok</th><th>Kategori</th><th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=0; foreach ($tampil_buku as $buku): $no++; ?>
				<tr>
					<td><?=$no?></td>
                    <td><?=$buku->judul_buku?></td>
                    <td><?=$buku->tahun?></td>
                    <td><?=$buku->harga?></td>
                    <td><img src="<?=base_url('asset/img/'.$buku->cover)?>" style="width: 80px"></td>
                    <td><?=$buku->penerbit?></td>
                    <td><?=$buku->penulis?></td>
                    <td><?=$buku->stok?></td>
                    <td><?=$buku->nama_kategori?></td>


					<td><a class="btn btn-warning" href="<?=base_url('index.php/transaksi/addcart/'.$buku->id_buku)?>">Beli</a></td>
				</tr>
			<?php endforeach ?> 
			
		</tbody>
	</table>
</div>
<div class="panel-heading">
			<h2 align="center">Shopping Cart</h2>
		</div>
		<?php if ($this->cart->contents()!=NULL): ?>
			<table 	id="example" class="table">
				<thead>
					<th>Id buku</th>
					<th>Judul buku</th>				
					<th>Harga</th>
					<th>Qty</th>
					<th>Aksi</th>
				</thead>
				<tbody>				
					<?php foreach ($this->cart->contents() as $items): ?>
						<tr>
							<td><?=$items['id']?></td>
							<td><?=$items['name']?></td>
							<td>Rp.<?=number_format($items['price'])?></td>	
							<td>
								<form method="POST" action="<?=base_url('index.php/transaksi/ubahqty/'.$items['rowid'])?>">
									<input onchange="submit()" name="qty" class="form-control" type="text" value="<?=$items['qty']?>">
								</form>
							</td>					
							<td><a class="btn btn-danger" href="<?=base_url('index.php/transaksi/hapus_cart/'.$items['rowid'])?>">x</a></td>
						</tr>

					<?php endforeach ?>
					<form method="POST" action="<?=base_url('index.php/transaksi/checkout')?>">
						<?php foreach ($this->cart->contents() as $items): ?>
							<input type="hidden" name="qty[]" value="<?=$items['qty']?>">
							<input type="hidden" name="id_buku[]" value="<?=$items['id']?>">
							<input type="hidden" name="stok[]" value="<?=$items['option']['stok']?>">	
						<?php endforeach ?>
						<tr style="background-color:red; color: white">
							<td colspan="2">GrandTotal</td><td>Rp. <?=number_format($this->cart->total())?></td><td colspan="2"></td>					
						</tr>
						<tr>
							<td>Uang Bayar :</td>

							<td>
								<input required type="number" name="uang" class="form-control">
							</td>

							<td>Pembeli :</td>
							<td>
								<input required type="text" name="nama_pembeli" class="form-control">
							</td>
							<td>
								<input type="submit" class="btn btn-success" name="bayar">
							</form>
						</td>
					</tr>
				</tbody>
			</table>		
		<?php else: ?>
			<center>Cart Kosong</center>
		<?php endif ?>	
	</div>
</div>