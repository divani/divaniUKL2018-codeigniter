<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Histori extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_histori','histor');
	}

	public function index()
	{
		if ($this->session->userdata('login')==TRUE) {
			$data['konten']='histori';
			$data['tampil_histori']=$this->histor->getDatahistor();
			$this->load->view('template', $data);
		}
		else{
			$this->load->view('login');
		}		
	}
	public function hapus($id_transaksi)
	{
		if ($this->histor->hapus($id_transaksi)) {
			$this->session->set_flashdata('pesan', 'Sukses Hapus Data');
		} else {
			$this->session->set_flashdata('pesan', 'Tidak berhasi dihapus, gagal');
		}
		redirect('histori','refresh');
	}

}

/* End of file Histori.php */
/* Location: ./application/controllers/Histori.php */