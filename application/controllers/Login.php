<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
	$this->load->view('login');
	}
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_login','user');
	}
	public function proses(){
		if ($this->input->post('cek')){
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]');
				if($this->form_validation->run()==TRUE){
					if($this->user->cek_user()->num_rows()>0){
						$data_user=$this->user->cek_user()->row();
						$array = array(
							'id_kasir'=> $data_user->id_kasir,
							'login'=> TRUE,
							'nama_kasir' => $data_user->nama_kasir,
							'password'=> $data_user->password,
							'level' => $data_user->level
							);
						$this->session->set_userdata($array);
							redirect('Toko','refresh');
						}
						else{
							$this->session->set_flashdata('pesan_salah','username / password salah');
							redirect('Login','refresh');
						}
					}else{
						$this->session->set_flashdata('pesan_eror', validation_error());
						redirect('Login');
					}

				}
			}
	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */	