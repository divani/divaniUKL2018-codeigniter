<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Controller {

	public function index()
	{		
	if($this->session->userdata('login')== TRUE){
		$data['konten']='home';
		$this->load->view('template', $data);
	}
	else{
		$this->session->flashdata('pesan_salah','Login terlebih dahulu');
		redirect('Login','refresh');
		}
	}


}

/* End of file Toko.php */
/* Location: ./application/controllers/Toko.php */