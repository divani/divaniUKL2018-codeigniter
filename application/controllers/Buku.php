<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_toko','toko');
	}
	public function index()
	{
		$data['tampil_kategori']=$this->toko->getDataKategori();
		$data['tampil_buku']=$this->toko->getDataBuku();
		if ($this->session->userdata('login')==TRUE) {
			$data['konten']='buku';
			$this->load->view('template',$data);		
		}
		else{
			$this->session->set_flashdata('pesan_salah', 
						'Login terlebih dahulu');
			redirect('Login','refresh');
		}	
	}
	public function tambah()
	{
		$this->form_validation->set_rules('judul_buku', 'judul buku', 'trim|required');
		$this->form_validation->set_rules('tahun', 'tahun', 'trim|required');
		$this->form_validation->set_rules('harga', 'harga', 'trim|required');
		$this->form_validation->set_rules('penerbit', 'penerbit', 'trim|required');
		$this->form_validation->set_rules('penulis', 'penulis', 'trim|required');
		$this->form_validation->set_rules('stok', 'stok', 'trim|required');
		$this->form_validation->set_rules('kategori', 'kategori', 'trim|required');
		if ($this->form_validation->run() == TRUE) {
			$config['upload_path'] = './asset/img/'; $config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '1000'; $config['max_width'] = '1024'; $config['max_height'] = '768';
			if($_FILES['cover']['name']!=""){
				$this->load->library('upload', $config);
				if (! $this->upload->do_upload('cover')){
					$this->session->set_flashdata('pesan', $this->upload->display_errors());
				} else {
					if($this->toko->simpan_buku($this->upload->data('file_name'))){
						$this->session->set_flashdata('pesan', 'sukses menambah');
					} else {
						$this->session->set_flashdata('pesan', 'gagal_menambah');
					}
					redirect('buku','refresh');
				}
			} else {
				if($this->buku->simpan_buku('')){
					$this->session->set_flashdata('pesan', 'sukses menambah');
				} else {
					$this->session->set_flashdata('pesan', 'gagal menambah');
				}
				redirect('buku','refresh');
			}
		} else {
			$this->session->set_flashdata('pesan', validation_errors());
			redirect('buku','refresh');
	}

	}
	public function hapus($id_buku=''){
		if($this->toko->hapus_buku($id_buku)){
			$this->session->set_flashdata('pesan', 'Sukses Hapus Buku');
			redirect('buku','refresh');
		}else{
			$this->session->set_flashdata('pesan','Gagal Hapus Buku');
			redirect('buku','refresh');
		}
	}
	public function edit_buku($id){
		$data=$this->toko->detail($id);
		echo json_encode($data);
	}
	public function buku_update()
	{
		if($this->input->post('simpan')){
			if($_FILES['cover']['name']==""){
				if($this->toko->buku_update_no_foto()){
					$this->session->set_flashdata('pesan', 'sukses update');
					redirect('buku');
				} else {
					$this->session->set_flashdata('pesan', 'gagal update');
					redirect('buku');
				}
			} else {
				$config['upload_path'] = './asset/img';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']  = '20000';
				$config['max_width']  = '5024';
				$config['max_height']  = '5768';
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('cover')){
					$this->session->set_flashdata('pesan', 'gagal upload');
					redirect('buku');
				} else{
					if($this->toko->buku_update_dengan_foto($this->upload->data('file_name'))){
						$this->session->set_flashdata('pesan', 'suskses update');
						redirect('buku');
					} else {
						$this->session->set_flashdata('pesan', 'gagal update');
						redirect('buku');
					}
				}
			}
		}
	}

}

/* End of file Buku.php */
/* Location: ./application/controllers/Buku.php */