<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_kategori','kategori');
	}
	public function index()
	{	
		$data['tampil_kategori']=$this->kategori->tampil_kategori();
		//tampilkategori untuk memanggil pada view
		if($this->session->userdata('login')== TRUE){
		$data['konten']='kategori'; //diambil dari nama view//
		$this->load->view('template', $data);
	}
	else{
		$this->session->flashdata('pesan_salah','Login terlebih dahulu');
		redirect('Login','refresh');
		}
	}
	public function tambah()
	{
		if ($this->input->post('simpan')) {
			if($this->kategori->simpan_kategori()){
				$this->session->set_flashdata('pesan', 'Tambah Kategori');
				redirect('kategori','refresh');
			}else{
				$this->session->set_flashdata('pesan', 'Gagal menyipan');
				redirect('kategori','refresh');
			}
		}
	}
	public function edit_kategori($id){
		$data=$this->kategori->detail($id);
		echo json_encode($data);
	}
	public function update_kategori(){
		if ($this->input->post('simpan')) {
			if($this->kategori->edit_kat()){
				$this->session->set_flashdata('pesan', 'Sukses Update Kategori');
				redirect('kategori','refresh');
			}else{
				$this->session->set_flashdata('pesan', 'Gagal Update Kategori');
				redirect('kategori','refresh');
			}
		}
	}

	public function hapus($id='')
	{
			if($this->kategori->hapus_kat($id)){
				$this->session->set_flashdata('pesan', 'Sukses Hapus Kategori');
				redirect('kategori','refresh');
			}else{
				$this->session->set_flashdata('pesan', 'Gagal Hapus menyimpan');
				redirect('kategori','refresh');
			}
		
	}
}